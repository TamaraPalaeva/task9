package ru.sberbank.impl;

import ru.sberbank.proxy.CacheInvocationHandler;
import ru.sberbank.service.Service;

import java.lang.reflect.Proxy;

public class ServiceImpl implements Service {

    private String name;
    private Integer value;

    public ServiceImpl() {
    }

    @Override
    public Service createWithCache(String path) {
        CacheInvocationHandler cacheInvocationHandler = new CacheInvocationHandler(this);
        ClassLoader classLoader = this.getClass().getClassLoader();
        Class[] interfaces = this.getClass().getInterfaces();
        return (Service) Proxy.newProxyInstance(classLoader, interfaces, cacheInvocationHandler);
    }

    @Override
    public Service createWithoutCache() {
        return this;
    }


    @Override
    public String doHardWorkTxt(String string, int size) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int j = 0; j < size; j++) {
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    @Override
    public String doHardWorkZip(String string, int size) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int j = 0; j < size; j++) {
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

    @Override
    public String doHardWorkMemory(String string, int size) {
        StringBuilder stringBuilder = new StringBuilder();
        for (int j = 0; j < size; j++) {
            stringBuilder.append(string);
        }
        return stringBuilder.toString();
    }

}
