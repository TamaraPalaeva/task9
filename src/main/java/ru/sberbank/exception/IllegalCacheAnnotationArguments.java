package ru.sberbank.exception;

public class IllegalCacheAnnotationArguments extends Exception {

    public IllegalCacheAnnotationArguments() {
        super("СacheType = IN_MEMORY и zip = true не может быть одновременно");
    }
}
