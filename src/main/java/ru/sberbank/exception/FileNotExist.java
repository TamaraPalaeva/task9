package ru.sberbank.exception;

public class FileNotExist extends Exception {

    public FileNotExist() {
        super("Файла не существует");
    }
}

