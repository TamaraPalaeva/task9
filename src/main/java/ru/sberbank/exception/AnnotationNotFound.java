package ru.sberbank.exception;

public class AnnotationNotFound extends Exception {

    public AnnotationNotFound() {
        super("Невозможно найти аннотацию");
    }
}