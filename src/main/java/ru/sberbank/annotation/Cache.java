package ru.sberbank.annotation;

import java.lang.annotation.*;

@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Cache {
    Type cacheType() default Type.FILE;

    String fileNamePrefix() default "cache";

    boolean zip() default false;

    Class[] identityBy() default {Object.class};

    int listList() default 1000;

}
