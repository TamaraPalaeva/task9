package ru.sberbank.annotation;

public enum Type {
    FILE,
    IN_MEMORY;
}
