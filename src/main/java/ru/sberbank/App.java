package ru.sberbank;


import ru.sberbank.impl.ServiceImpl;
import ru.sberbank.service.Service;

public class App {

    static Service service = new ServiceImpl();

    public static void main(String[] args) throws NoSuchMethodException {

        Service serviceWith = service.createWithCache("");
        Service serviceWithout = service.createWithoutCache();

        System.out.println("TXT");
        String txt1 = serviceWith.doHardWorkTxt("work1", 10);
        System.out.println(txt1);
        String txt2 = serviceWith.doHardWorkTxt("work2", 5);
        System.out.println(txt2);
        String txt3 = serviceWith.doHardWorkTxt("work1", 5);
        System.out.println(txt3);

        System.out.println("\nZIP");
        String zip1 = serviceWith.doHardWorkZip("work1", 10);
        System.out.println(zip1);
        String zip2 = serviceWith.doHardWorkZip("work2", 5);
        System.out.println(zip2);
        String zip3 = serviceWith.doHardWorkZip("work1", 10);
        System.out.println(zip3);

        System.out.println("\nMEMORY");
        String memory1 = serviceWith.doHardWorkMemory("work1", 10);
        System.out.println(memory1);
        String memory2 = serviceWith.doHardWorkMemory("work2", 5);
        System.out.println(memory2);
        String memory3 = serviceWith.doHardWorkMemory("work1", 5);
        System.out.println(memory3);
//


    }

}
