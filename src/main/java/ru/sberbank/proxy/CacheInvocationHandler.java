package ru.sberbank.proxy;


import ru.sberbank.annotation.Cache;
import ru.sberbank.annotation.Type;
import ru.sberbank.exception.AnnotationNotFound;
import ru.sberbank.exception.FileNotExist;
import ru.sberbank.exception.IllegalCacheAnnotationArguments;
import ru.sberbank.service.Service;

import java.io.*;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;
import java.util.zip.ZipOutputStream;

public class CacheInvocationHandler implements InvocationHandler {
    private Service service;
    private static Map<Object, Object> cache = new WeakHashMap<>();

    public CacheInvocationHandler(Service service) {
        this.service = service;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {

        Object object;
        Cache annotation = method.getAnnotation(Cache.class);

        if (annotation != null) {
            Type cacheType = annotation.cacheType();
            if (cacheType == Type.FILE) {
                String fileName = fileNameCreate(method, args, annotation);
                boolean zip = annotation.zip();
                File file = new File(fileName + "txt");
                if (zip) {
                    return zipFileWork(fileName, method, args, file);
                } else {
                    return txtFileWork(method, args, file);
                }
            } else if (cacheType == Type.IN_MEMORY && !annotation.zip()) {
                String fileName = fileNameCreate(method, args, annotation);
                if (cache.containsKey(fileName)) {
                    System.out.println("Результат существует в кэше");
                    return cache.get(fileName);
                } else {
                    object = method.invoke(service, args);
                    if (method.getReturnType().equals(List.class)) {
                        List workList = (List) object;
                        Object limitedList = null;
                        int listList = method.getAnnotation(Cache.class).listList();
                        if (workList.size() > listList) {
                            limitedList = workList.stream()
                                    .limit(listList)
                                    .collect(Collectors.toList());
                        }
                        cache.put(fileNameCreate(method, args, annotation), limitedList);
                    } else {
                        cache.put(fileNameCreate(method, args, annotation), object);
                    }
                    System.out.println("В кеш создана новая запись. Количество записей: " + cache.size());
                    return object;
                }
            } else throw new IllegalCacheAnnotationArguments();

        } else throw new AnnotationNotFound();
    }


    private String fileNameCreate(Method method, Object[] args, Cache anotation) {
        String fileNamePrefix = anotation.fileNamePrefix();
        Class[] identityBy = anotation.identityBy();
        StringBuilder stringBuilder = new StringBuilder();
        stringBuilder.append(fileNamePrefix + "." + method.getName() + ".");
        for (Class cl : identityBy) {
            for (Object obj : args) {
                if (cl.isInstance(obj)) {
                    stringBuilder.append(obj.toString());
                    stringBuilder.append(".");
                }
            }
        }
        return stringBuilder.toString();
    }

    private Object zipFileWork(String fileName, Method method, Object[] args, File file)
            throws IOException, InvocationTargetException, IllegalAccessException, ClassNotFoundException, FileNotExist {

        Object object = null;
        File zipFile = new File(fileName + "zip");
        if (zipFile.exists()) {
            try (ZipInputStream zipInputStream = new ZipInputStream(new FileInputStream(zipFile))) {
                ZipEntry zipEntry = zipInputStream.getNextEntry();
                if (zipEntry != null) {
                    try (ObjectInputStream o = new ObjectInputStream(zipInputStream)) {
                        System.out.println("Чтение из зип файла");
                        return o.readObject();
                    }
                } else {
                    throw new FileNotExist();
                }
            }

        } else {
            try (ZipOutputStream zipOutputStream = new ZipOutputStream(new FileOutputStream(zipFile))) {
                writeTxt(method, args, file);
                try (FileInputStream fileInputStream = new FileInputStream(file)) {
                    ZipEntry zipEntry = new ZipEntry(file.getName());
                    zipOutputStream.putNextEntry(zipEntry);
                    byte[] buffer = new byte[fileInputStream.available()];
                    fileInputStream.read(buffer);
                    zipOutputStream.write(buffer);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            Files.delete(Paths.get(file.getPath()));
            return "Запись в файл " + zipFile;
        }
    }

    private Object txtFileWork(Method method, Object[] args, File file) throws Exception {
        if (file.exists()) {
            System.out.println("Чтение из файл");
            return readTxt(file);
        } else {
            System.out.println("Запись в файла");
            return writeTxt(method, args, file);
        }
    }

    private Object writeTxt(Method method, Object[] args, File file) throws Exception {
        Object object;
        try (ObjectOutputStream o = new ObjectOutputStream(new FileOutputStream(file))) {
            object = method.invoke(service, args);
            if (method.getReturnType().equals(List.class)) {
                List invokedList = (List) object;
                int listList = method.getAnnotation(Cache.class).listList();
                if (invokedList.size() > listList) {
                    Object limitedList = invokedList.stream().limit(listList).collect(Collectors.toList());
                    o.writeObject(limitedList);
                }
            } else {
                o.writeObject(object);
            }
            return object;
        }
    }

    private Object readTxt(File file) throws Exception {
        Object object;
        try (ObjectInputStream o = new ObjectInputStream(new FileInputStream(file))) {
            object = o.readObject();
            return object;
        }
    }
}

