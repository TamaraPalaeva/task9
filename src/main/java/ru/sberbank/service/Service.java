package ru.sberbank.service;

import ru.sberbank.annotation.Cache;

import static ru.sberbank.annotation.Type.FILE;
import static ru.sberbank.annotation.Type.IN_MEMORY;

public interface Service {

    Service createWithCache(String path);

    Service createWithoutCache();


    @Cache(cacheType = FILE, identityBy = {String.class})
    String doHardWorkTxt(String s, int i);

    @Cache(cacheType = FILE, zip = true, identityBy = {String.class, Integer.class})
    String doHardWorkZip(String s, int i);

    @Cache(cacheType = IN_MEMORY, identityBy = {String.class})
    String doHardWorkMemory(String s, int i);

}
